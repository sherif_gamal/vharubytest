require_relative '../lib/bowling'

loop do
  print "Enter sequence or q to exit> "
  sequence = gets.chomp

  exit if (sequence == "" || sequence == "q")

  sequence = sequence.split(" ");
  sequence = sequence.map { |s| s.to_i }
  b = Bowling.new(sequence)
  puts b.score
end
