require_relative '../lib/bowling'

describe Bowling do

  context "#initialize" do
    it "should create an object if input is valid" do
      b = Bowling.new([1, 2, 3, 4])
      expect(b).to be_a(Bowling)
    end

    it "should throw an error if a any throw is more than 10" do
      expect {
        b = Bowling.new([1, 2, 30, 4])
      }.to raise_error("You can't score more than 10 in each bowl! Found 30")
    end

    it "should throw an error if second throw gives 10 except for the last frame" do
      expect {
        b = Bowling.new([1, 2, 3, 10])
      }.to raise_error("Found 10 as the second bowl in a frame, this can't happen!")
    end

    it "should throw an error if any frame score exceeds 10 except for the last frame" do
      expect {
        b = Bowling.new([1, 2, 3, 9])
      }.to raise_error("Frame scores cannot exceed 10!")
    end

    it "should succeed with normal sequence (no strikes and no spares)" do
      b = Bowling.new([1, 2, 3, 4])
      expect(b.score).to eq(10)
    end

    it "should succeed with spares" do
      b = Bowling.new([9, 1, 9, 1])
      expect(b.score).to eq(29)
    end

    it "should succeed with strikes" do
      b = Bowling.new([1, 1, 1, 1, 10, 1, 1])
      expect(b.score).to eq(18)
    end

    it "should succeed with strikes" do
      b = Bowling.new([10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10])
      expect(b.score).to eq(300)
    end

    it "should ignore anything more than 10 frames" do
      b = Bowling.new([10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10])
      expect(b.score).to eq(300)
    end
  end



end
