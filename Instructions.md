Please do the following to run the code:
========================================
1- cd to the project directory
2- run "ruby bin/bowling_score.rb"
This should prompt you for input, enter the sequence of numbers as comma separated sequence e.g. 10 10 10 10 10 10 10 10 10 10 10 10
when you are done, hit Return or input q to exit.

To run the tests:
=================
1- cd to the project directory
2- run "rspec"
