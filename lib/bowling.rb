class Bowling
  def initialize(nums)
    @seq = []
    frame = []
    # convert the sequence into a sequence of arrays of size 2 each, insert 0 after each 10 and raise error if sequence is invalid
    nums.each do |num|
      raise "You can't score more than 10 in each bowl! Found #{num}" if num > 10
      if @seq.length == 9
        frame << num
      else
        if num < 10
          # normal number, add it to the current frame
          frame << num
        elsif num == 10
          # 10 can only happen in the beginning of a frame, add 0 as the next score in frame
          if frame.empty?
            frame = [10, 0]
          else
            raise "Found 10 as the second bowl in a frame, this can't happen!"
          end
        end
      end

      # add complete frames to the sequence
      if @seq.length < 9 && frame.length == 2
        if frame.reduce(:+) > 10
          raise "Frame scores cannot exceed 10!"
        end
        @seq << frame
        frame = []
        # the last frame has three throws
      end
    end
    if frame.length > 0
      (3 - frame.length).times do
        frame << 0 #append 0's if last frame is less than 3
      end
      @seq << frame.slice(0, 3) # Ignore any input more than 10 frames
    end
  end

  def score
    # @seq is like [[1,2], [3, 1], [10, 0], [5, 5]]
    # take only first 10 frames and ignore all else
    total_score = 0
    extras = [0, 0]
    @seq.each_with_index do |s, i|
      frame_sum = 0
      s.each do |n|
        frame_sum += n

        if n > 0
          total_score += n * extras.shift
          extras << 0
        end

      end
      if s[0] == 10
        extras[0] += 1
        extras[1] += 1
      elsif frame_sum == 10
        extras[0] += 1
      end
      total_score += frame_sum
    end

    total_score
  end
end

# b = Bowling.new([1, 2, 3, 4])
# puts b.score
# b = Bowling.new([9, 1, 9, 1])
# puts b.score
# b = Bowling.new([1, 1, 1, 1, 10, 1, 1])
# puts b.score
# b = Bowling.new([10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10])
# puts b.score
